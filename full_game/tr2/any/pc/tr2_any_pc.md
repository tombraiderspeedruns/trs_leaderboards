[General Rules](https://gitlab.com/tombraiderspeedrunning/trs_records/blob/master/rules/rules_general.md)

[Any% Rules](https://gitlab.com/tombraiderspeedrunning/trs_records/blob/master/rules/rules_any.md)

[Additional Rules](additional_rules.md)

Player   |  Time          |  Game Version  |  Platform  |  Date        |  Video Link
:-------:|:--------------:|:--------------:|:----------:|:------------:|:-------------------------------------------:
Daren_K  |  00:55:05.800  |  Original      |  PC        |  2019-09-20  |  https://www.youtube.com/watch?v=RdrpZDejPIc

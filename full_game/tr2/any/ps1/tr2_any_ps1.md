[General Rules](https://gitlab.com/tombraiderspeedrunning/trs_records/blob/master/rules/rules_general.md)

[Any% Rules](https://gitlab.com/tombraiderspeedrunning/trs_records/blob/master/rules/rules_any.md)

Player   |  Time          |  Game Version  |  Platform  |  Date        |  Video Link
:-------:|:--------------:|:--------------:|:----------:|:------------:|:-------------------------------------------:
Daren_K  |  00:53:16.333  |  SLES00718     |  PS2       |  2019-02-07  |  https://www.youtube.com/watch?v=6Ke-zmvVnsI

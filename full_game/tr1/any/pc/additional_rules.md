Tomb Raider I - Additional Rules
=================================

## Game Versions
A list of confirmed game versions can be found [[here]](https://gitlab.com/tombraiderspeedrunning/trs_version_database/tree/master/tr1/pc).

## Information on the ATI patch [[tombatiragepro]](https://gitlab.com/tombraiderspeedrunning/trs_version_database/tree/master/tr1/pc/patches/eidos-france.fr/tombatiragepro)
It's currently impossible to finish the game with this version as the game crashes on the exploding death of the enemies Centaur Mutant and Giant Mutant unless a Boss Skip or a Credits Skip is found. This version contains a bunch of additional issues that could prove useful to break the game on the instruction-level. You can try it out without game-changing modifications on modern Windows versions using the TRS fork of the [[glrage]](https://gitlab.com/tombraiderspeedrunning/glrage/tree/trs) wrapper.

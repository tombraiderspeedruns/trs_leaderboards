[General Rules](https://gitlab.com/tombraiderspeedrunning/trs_records/blob/master/rules/rules_general.md)

[Any% Rules](https://gitlab.com/tombraiderspeedrunning/trs_records/blob/master/rules/rules_any.md)

Player   |  Time          |  Game Version  |  Platform  |  Date        |  Video Link
:-------:|:--------------:|:--------------:|:----------:|:------------:|:-------------------------------------------:
Daren_K  |  01:06:52.000  |  SLUS00152     |  PSTV      |  2018-09-01  |  https://www.youtube.com/watch?v=E9TelbuaPAQ

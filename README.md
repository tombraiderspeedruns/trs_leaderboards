Tomb Raider Speedrunning - Records
=====================================

Records of the Tomb Raider Speedrunning community.
[TRS Discord Server](https://discord.gg/KV6brta)

## Rules

[General submission rules](https://gitlab.com/tombraiderspeedrunning/trs_records/tree/master/rules/rules_general.md)

## Any%

[General Any% rules](https://gitlab.com/tombraiderspeedrunning/trs_records/tree/master/rules/rules_any.md)

Game | DC | Mac | Mobile | PC | PS1 | Saturn
:---:|:--:|:---:|:------:|:--:|:---:|:-----:
TR1 | n/a | - | n/a | [01:07:25](https://gitlab.com/tombraiderspeedrunning/trs_records/tree/master/full_game/tr1/any/pc/tr1_any_pc.md) | [01:06:52](https://gitlab.com/tombraiderspeedrunning/trs_records/tree/master/full_game/tr1/any/ps1/tr1_any_ps1.md) | [01:04:17](https://gitlab.com/tombraiderspeedrunning/trs_records/tree/master/full_game/tr1/any/saturn/tr1_any_saturn.md)
TR2 | n/a | - | - | [00:55:05.800](https://gitlab.com/tombraiderspeedrunning/trs_records/tree/master/full_game/tr2/any/pc/tr2_any_pc.md) | [00:53:16.333](https://gitlab.com/tombraiderspeedrunning/trs_records/tree/master/full_game/tr2/any/ps1/tr2_any_ps1.md) | n/a

## Secrets%

[General Secrets% rules](https://gitlab.com/tombraiderspeedrunning/trs_records/tree/master/rules/rules_secrets.md)

No runs yet.

## 100%

[General 100% rules](https://gitlab.com/tombraiderspeedrunning/trs_records/tree/master/rules/rules_100.md)

No runs yet.

## See Also

* [TRS Resources](https://gitlab.com/tombraiderspeedrunning/trs_resources)
* [TRS Version Database](https://gitlab.com/tombraiderspeedrunning/trs_version_database)

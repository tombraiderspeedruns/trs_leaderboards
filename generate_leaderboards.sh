#!/usr/bin/env bash

for f in `find . -name '*.csv'`
do
	md_file="${f%".csv"}.md"

	echo -e "[General Rules](https://gitlab.com/tombraiderspeedrunning/trs_records/blob/master/rules/rules_general.md)\n" > ${md_file}

	if [[ ${f} == *"any"* ]]; then
		echo -e "[Any% Rules](https://gitlab.com/tombraiderspeedrunning/trs_records/blob/master/rules/rules_any.md)\n" >> ${md_file}
	fi

	if [[ ${f} == *"secrets"* ]]; then
		echo -e "[Secrets% Rules](https://gitlab.com/tombraiderspeedrunning/trs_records/blob/master/rules/rules_secrets.md)\n" >> ${md_file}
	fi

	if [[ ${f} == *"100"* ]]; then
		echo -e "[100% Rules](https://gitlab.com/tombraiderspeedrunning/trs_records/blob/master/rules/rules_100.md)\n" >> ${md_file}
	fi

	if [ -f "$(dirname ${f})/additional_rules.md" ]; then
		echo -e "[Additional Rules](additional_rules.md)\n" >> ${md_file}
	fi

	csvtomd "${f}" | sed -e 's/--/:-/1' -e 's/-|-/:|:/g' -e 's/-$/:/1g' >> ${md_file}
done
